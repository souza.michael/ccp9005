#!/usr/bin/env python
# coding: utf-8

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
import multiprocessing as mp
import cProfile
from sklearn.linear_model import LinearRegression


def create_graph_1D(nnodes):
    G = nx.Graph()
    G.add_node(0)
    right = 0
    left = 0
    for _ in range(int(nnodes/2)):
        G.add_edge(right + 1, right)
        G.add_edge(left, left - 1)
        right = right + 1
        left = left - 1
    root = 0
    return G, root


def create_graph_2D(nnodes):
    L = int(np.sqrt(nnodes) / 2)
    print('L = %d' % L)
    G = nx.Graph()
    root = (0, 0)
    G.add_node(root)
    for i in range(-L, L + 1):
        for j in range(-L, L + 1):
            v = (i, j)
            G.add_node(v)
            if abs(i) > 0:
                x = i - i / abs(i), j
                G.add_edge(v, x)
            if abs(j) > 0:
                y = i, j - j / abs(j)
                G.add_edge(v, y)
    return G, root


def move_uniform(G, v):
    V = list(G.neighbors(v))
    i = np.random.randint(0, len(V))
    u = V[i]
    return u


def random_walk(G, root, move, nsteps, idx, W):
    # Sets the i-th walker started in W[idx * nsteps]
    v = root
    for i in range(nsteps):
        v = move(G, v)
        W[idx * nsteps + i] = v


def plot_graph(G):
    # TODO Use coordinates
    nx.draw(G, with_labels=True)
    plt.show()


def histogram(x, bins):
    # Histogram of relative frequency density
    x = np.sort(x)

    for i in range(len(bins) - 1):
        if bins[i] > bins[i+1]:
            raise Exception('The bins edges are not sorted')
    if x[0] < bins[0] or x[-1] > bins[-1]:
        raise Exception('There are values out of the bins limits')

    k = 0
    f = np.zeros(len(bins) - 1)  # frequency
    for i in range(len(x)):
        if x[i] >= bins[k+1]:
            k = k + 1
        f[k] = f[k] + 1

    fd = np.zeros(len(f))  # relative frequency density
    z = np.zeros(len(f))
    for i in range(len(f)):
        fd[i] = f[i] / ((bins[i+1] - bins[i]) * len(x))
        z[i] = 0.5 * (bins[i] + bins[i+1])
    return z, fd


def norm_pdf(x, mu, sigma):
    y = np.exp(-(x-mu)**2/(2*sigma**2)) / (np.sqrt(2*np.pi) * sigma)
    return y


def write_csv_vector(fname, v):
    with open(fname, 'w') as fid:
        print('Writing file %s' % fname)
        for i in range(len(v)):
            fid.write('%g\n' % v[i])

def walkers(nnodes, nsteps, nwalks, graph, move):
    G, root = graph(nnodes)
    
    W = np.zeros(nsteps * nwalks, dtype=int)
    for idx in range(nwalks):
        random_walk(G, root, move, nsteps, idx, W) 
    W = W.reshape((nwalks, nsteps))

    return W


def plot_R(W):
    # Plot mean distance vs step
    R=np.zeros((len(W), len(W[0])))
    for i in range(len(W)):
        for j in range(len(W[i])):
            R[i][j]=abs(W[i][j])
    r=R.mean(0)
    t=np.arange(1, len(r) + 1)
    t=t[r > 0]
    r=r[r > 0]
    plt.loglog(t, r)
    plt.xlabel('log(time)')
    plt.ylabel('log(<r>)')
    return t, r


def plot_M(W):
# Histogram normalized (x - <x>)/std(x) of the final state
    nsteps=len(W[0]) - 1

    x=np.array([W[i][-1] for i in range(len(W))])
    mu=np.mean(x)
    sigma=np.std(x)

    # bins used by histogram
    # step = 2, because P(m) = 0 if mod(m,2) neq mod(N,2)
    bins=[np.min(x) - 1]
    h=[]  # bin size
    for _ in range(np.min(x), np.max(x) + 1, 2):
        bins.append(bins[-1] + 2)
        h.append(bins[-1] - bins[-2])
    bins=np.array(bins)
    h=np.array(h)

    m, pm=histogram(x, bins)

    # scaling by (x - mu) / sigma, where mu is the mean
    # and sigma the standard deviation
    print('mu = %g, sigma = %g\n' % (mu, sigma))
    m=(m - mu)/sigma
    pm=pm * sigma
    plt.plot(m, pm, 'o', label = 't=%d' % nsteps)

    return m, pm


def plot_Gaussian(mu, sigma):
    z=np.linspace(-3 * sigma, 3 * sigma)
    y=norm_pdf(z, mu, sigma)
    plt.plot(z, y, '--', label = 'N(0,1)')
    plt.xlabel('m')
    plt.ylabel('P(m)')


def linear_regression(x, y):
# calculates coeficients 'a' and 'b' for y = a * x + b
    X=np.array([[x[i], y[i]] for i in range(len(x))])
    R=LinearRegression()
    print('x = ', x)
    print('y = ', y)
    R=R.fit(X)
    a=R.coef_
    b=R.intercept_
    return a, b

# G, root = create_graph_2D(15)
# plot_graph(G)

# profile = cProfile.Profile()
# profile.enable()


# M = walkers(100, 100, 10000, create_graph_1D, move_uniform)
W=walkers(nnodes = 100, nsteps = 100, nwalks = 10000,
            graph = create_graph_1D, move = move_uniform)

# profile.disable()
# profile.print_stats()

# t, r = plot_R(W)
# plt.show()

# plot_M(W)
# plot_Gaussian(0, 1)
# plt.show()

# with open('data.csv', 'w') as fid:
# for i in range(len(t)):
# fid.write('%g, %g\n' % (t[i], r[i]))
# plt.plot(t, 0.8 * np.sqrt(t), '--')
# plt.show()

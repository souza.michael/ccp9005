C******>ESTE PROGRAMA CALCULA O CLUSTER INFINITO PARA UMA REDE QUADRADA 
C******>CALCULA O PC DE PERCOLACAO PARA SITIOS E AS RESPECTIVAS SEMENTEA PARA
C******>ESTE PC UTILIZANDO O GERADOR RAN48(SEED) CALCULA O TAMANHO DO CLUSTER
C******>INFINITO EM PC E FAZ O GRAFICO DE M(L) X L 

	PROGRAM PERCO
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	INTEGER*2 SEED(3),SEED0(3)
	PARAMETER (N=1000)
c	OPEN(85,FILE='mass_cluinf_s_1000.dat')
c	OPEN(80,FILE='aq_mass_cluinf_s_1000.dat')
	NFSEM=10
	PMAX=1.0D0
	PMIN=0.0D0
	PER=1.D-4
	NSEM=1
	PCS=0.D0
	PCM=0.D0
	MTC=0
	WRITE(*,*)'INICIOU PROGRAMA'
	SEED(1)=643
	SEED(2)=758
	SEED(3)=637
 300    IF(NSEM.GT.NFSEM) GO TO 340
	MMM=0
	SEED0(1)=SEED(1)
	SEED0(2)=SEED(2)
	SEED0(3)=SEED(3)
	PDO=PMIN
	PUP=PMAX
 310	PAV=(PDO+PUP)/2.D0
C	write(*,*) pav
	CALL CONTA(PAV,KK,SEED,MMM,MTC)
	IF(KK.EQ.1.D0) THEN
	   IF(DABS(PAV-PDO).LT.PER) THEN
	      PCRI=PAV
	      DO 7 I=1,3 
C		 WRITE(80,*)SEED0(I)
c                 WRITE(6,*)SEED0(I)
 7	      CONTINUE
C             WRITE(*,*)PCRI
	      MMM=1
	      CALL CONTA(PAV,KK,SEED,MMM,MTC)
**********> PCS==> probabilidade critica somada a cada rede
c	      PCS=PCS+PCRI
	      IF((NSEM/10)*10.EQ.NSEM) WRITE(*,*) NSEM
	      WRITE(*,*) NSEM,PCRI
	      NSEM=NSEM+1
	      GO TO 300
	   ELSE
	      PUP=PAV
              	SEED(1)=SEED0(1)
		SEED(2)=SEED0(2)
		SEED(3)=SEED0(3)
	      GO TO 310
	   END IF
	ELSE
	   IF(DABS(PUP-PAV).LE.PER) THEN
	      PCRI=PUP
	      DO 8 I=1,3 
C		 WRITE(80,*)SEED0(I)
C                WRITE(6,*)SEED0(I)
 8	      CONTINUE
c	      WRITE(*,*)nsem,pcri
	      MMM=1
	      CALL CONTA(PAV,KK,SEED,MMM,MTC)
C***********> PCS==> probabilidade critica somada a cada rede
c	      WRITE(*,*)nsem,pcri
	      PCS=PCS+PCRI	
	      IF((NSEM/10)*10.EQ.NSEM) WRITE(*,*) NSEM
c	      WRITE(*,*) NSEM,PCRI
	      NSEM=NSEM+1
	      GO TO 300
	   ELSE
	      PDO=PAV
              	SEED(1)=SEED0(1)
		SEED(2)=SEED0(2)
		SEED(3)=SEED0(3)
	      GO TO 310
	   END IF
	END IF
 340	PCM=PCS/DFLOAT(NFSEM)
	AMC=float(MTC/NFSEM)
	WRITE(85,*)dlog10(AMC),dlog10(DFLOAT(N)),PCM,AMC
c	WRITE(*,*)log10(AMC),log10(100.d0)
c	WRITE(*,*) 'probabilidade media',pcm,amc
	CLOSE(85)
	END
C
C
C
	SUBROUTINE  CONTA(PROB,KK,SEED,MMM,MTC)
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)
	INTEGER*2 SEED(3)
	PARAMETER (N=500)
	COMMON/INT/LAB1,LAB2,LAB3,LAB4
	DIMENSION NN(N*N+N),NC(N*N+N),T(2*N*N+N)
	DIMENSION CA(2*N*N+N),RE(2*N*N+N),LAB(N*N)
C	OPEN(20,FILE='posc8nc.dat')
	NLIG=2*N*N+N
	NNN=N*N
	RE1=1.D0
	RE2=0.D0
	CA1=1.D0
	CA2=0.D0
C
C*******>ESTA PRIMEIRA PARTE TRANSFORMA PERCOLACAO DE SITIO EM PERCOLACAO DE C*******> LIGACAO
C        
	 DO 105 K=1,NNN
	      PSIT=RAND48(SEED)
	      IF(PSIT.LT.PROB) THEN
		 LAB(K)=1
	      ELSE
		 LAB(K)=0
	      END IF
 105	   CONTINUE   
C       
C********> CONSIDERA AS PRIMEIRAS N  LIGACOES
C       	   
	   
	   KLIG=0
	   DO 25 I=1,N
	      KLIG=KLIG+1
	      IF(LAB(I).EQ.1.D0) THEN
		 RE(KLIG)=RE1
		 CA(KLIG)=CA1
	      ELSE
		 RE(KLIG)=RE2
		 CA(KLIG)=CA2
	      END IF
 25	   CONTINUE
	   
C       
C*******> CONSIDERA AS LIGACOES PARA BAIXO
C       
	
	
	   DO 35 M=1,N
	      DO 45 L=1,N
		 IF(M.EQ.N) THEN
		    KLIG=KLIG+1
		    IF(LAB((N*(M-1))+L).EQ.1) THEN
		       RE(KLIG)=RE1
		       CA(KLIG)=CA1
		    ELSE
		       RE(KLIG)=RE2
		       CA(KLIG)=CA2
		    END IF
		    
		 ELSE
		    KLIG=KLIG+1
		    IF(LAB((N*(M-1))+L).EQ.1.AND.LAB((N*(M-1))+L+N).EQ.1) THEN
		       RE(KLIG)=RE1
		       CA(KLIG)=CA1
		    ELSE
		       RE(KLIG)=RE2
		       CA(KLIG)=CA2
		    END IF
		 END IF
c       write(*,*) re(klig),ca(klig),klig,'##############'
C       
C**********> CONSIDERA AS LIGACOES PARA FRENTE
C       
		 IF(L.EQ.N) THEN
		    KLIG=KLIG+1
		    LA=(N*(M-1)+L)
		    LB=(N*(M-1)+L+1-N)
		    IF(LAB(LA).EQ.1.AND.LAB(LB).EQ.1)THEN
		       RE(KLIG)=RE1
		       CA(KLIG)=CA1
		    ELSE
		       RE(KLIG)=RE2
		       CA(KLIG)=CA2
		    END IF
		    
		 ELSE
		    KLIG=KLIG+1
		    IF(LAB(N*(M-1)+L).EQ.1.AND.LAB(N*(M-1)+L+1).EQ.1) THEN
  		       RE(KLIG)=RE1
		       CA(KLIG)=CA1
		    ELSE
		       RE(KLIG)=RE2
		       CA(KLIG)=CA2
		    END IF
		 END IF
		 
 45	      CONTINUE
 35	   CONTINUE
	   
C*******> ESTA SEGUNDA PARTE IDENTIFICA O CLUSTER DE PERCOLACAO 
	
C
C
C
	   DO 10 M=1,N+1
	      DO 15 L=1,N
		 NN((M-1)*N+L)=((M-1)*N+L)
 15	      CONTINUE
 10	   CONTINUE
	   
C       
C********> LIGACOES PARA BAIXO
C       
	   K=N-1
	   DO 20 M=1,N
	      DO 30 L=1,N
		 K=K+2
		 T(K)=RE(K)+CA(K)*S
		 IF(T(K).NE.0.D0)THEN
		    IF(NN((M-1)*N+L).LE.NN((M-1)*N+L+N))THEN
		       NN((M-1)*N+L+N)=NN((M-1)*N+L)
		    END IF
		 END IF
 30	      CONTINUE
 20	   CONTINUE
C       
C*********> LIGACOES PARA FRENTE
C       
	   K=N
	   DO 40 M=1,N
	      DO 50 L=1,N
		 K=K+2
		 T(K)=RE(K)+CA(K)*S
		 IF(T(K).NE.0.D0)THEN
		    IF(L.EQ.N) THEN
		       LAB1=NN((M-1)*N+L)
		       LAB2=NN((M-1)*N+L+1-N)
 60		       CONTINUE
		       IF(LAB1.NE.NN(LAB1))THEN
			  LAB1=NN(LAB1)
			  GO TO 60
		       END IF
 70		       CONTINUE
		       IF(LAB2.NE.NN(LAB2))THEN
			  LAB2=NN(LAB2)
			  GO TO 70
		       END IF
		       IF(LAB1.LE.LAB2)THEN
			  NN(LAB2)=LAB1
		       ELSE
			  NN(LAB1)=LAB2
		       END IF
		    ELSE
		       LAB1=NN((M-1)*N+L)
		       LAB2=NN((M-1)*N+L+1)
 80		       CONTINUE
		       IF(LAB1.NE.NN(LAB1))THEN
			  LAB1=NN(LAB1)
			  GO TO 80
		       END IF
 90		       CONTINUE
		       IF(LAB2.NE.NN(LAB2))THEN
			  LAB2=NN(LAB2)
			  GO TO 90
		       END IF
		       IF(LAB1.LE.LAB2)THEN
			  NN(LAB2)=LAB1
		       ELSE
			  NN(LAB1)=LAB2
		       END IF
		    END IF
		 END IF
 50	      CONTINUE
 40	   CONTINUE
c	do 201 i=1,6
c	   WRITE(*,*)(nn((i-1)*5+j),j=1,5)
c 201	continue
c	write(*,*) '#####################'
C       
C***********> ULTIMA CLASSIFICACAO
C       
	   DO 150 I=1,NNN+N
	      LAB4=NN(I)
 140	      CONTINUE
	      IF(NN(LAB4).NE.LAB4) THEN
		 LAB4=NN(LAB4)
		 GO TO 140
	      ENDIF
	      NN(I)=LAB4
	      NC(LAB4)=NC(LAB4)+1
 150	   CONTINUE
C       
C***********> PRIMEIRAS LIGACOES
C       
	   K=0
	   KK=0
	   DO 100 L=1,N
	      K=K+1
	      T(K)=RE(K)+CA(K)*S
	      IF(T(K).NE.0.D0)THEN
		 DO 120 LL=1,N
		    IF(NN(L).EQ.NN(LL+NNN))THEN
C		       WRITE(*,*)NN(L)
		       LAB5=NN(L)
		       KK=1
C		       STOP
		    ELSE
		    END IF
 120		 CONTINUE
	      END IF
 100	   CONTINUE
C       
C       
C       
C	   DO 200 K=1,NLIG
C	      IF(T(K).EQ.0.D0)THEN
C		 WRITE(20,*) 0
C	      ELSE
C		 WRITE(20,*) 1
C	      END IF
C 200	   CONTINUE
C       
C       
C  
	IF(MMM.EQ.1)THEN
	   MCLUST=0
	   DO 220 L=1,NNN
	      IF(NN(L).EQ.LAB5)THEN
C		 WRITE(20,*) 1
		 MCLUST=MCLUST+1
	      ELSE
C		 WRITE(20,*) 0
	      END IF
 220	   CONTINUE
	   write(80,*)mclust
	   MTC=MTC+MCLUST 
C	   CLOSE(20)
	   ELSE
	END IF
 222	   RETURN	   	
	   END		
C
C
C
C 	
c program rand48.f:

      FUNCTION rand48(seed)
c
c                                Fortran 77 function that implements
c                                the random number generator used
c                                by the C utility erand48. It produces
c                                the same values as erand48 for the
c                                three integers seed(3) and a sequence
c                                of random numbers that agree in all but
c                                the least significant digits. 
c                                (Very small discrepancies in the least 
c                                significant digits are produced by the 
c                                different truncations used by Fortran
c                                and C.)   
c
c                                        Claudio Rebbi - Boston University
c                                                        May 1992 
c
      INTEGER*2 seed(3)
      INTEGER*4 i1,i2,i3,i11,i21,i31,i12,i22,i13
      INTEGER*4 E66D,DEEC,FFFF
      PARAMETER(E66D=58989, DEEC=57068, FFFF=65535)
      REAL*8 rand48

      i1=seed(1)
      IF(i1.LT.0) i1=i1+65536
      i2=seed(2)
      IF(i2.LT.0) i2=i2+65536
      i3=seed(3)
      IF(i3.LT.0) i3=i3+65536

        i11=i1*E66D
        i21=i2*E66D
        i31=i3*E66D
        i12=i1*DEEC
        i22=i2*DEEC
        i13=i1*5
        i1=IAND(i11,FFFF)+11
        i11=ISHFT(i11,-16)+ISHFT(i1,-16)
        i1=IAND(i1,FFFF)
        i11=i11+IAND(i21,FFFF)+IAND(i12,FFFF)
        i2=IAND(i11,FFFF)
        i3=ISHFT(i11,-16)+ISHFT(i21,-16)+ISHFT(i12,-16)+
     &     IAND(i31,FFFF)+IAND(i22,FFFF)+IAND(i13,FFFF)
        i3=IAND(i3,FFFF)
c
c       rand48=i3*2**(-16)+i2*2**(-32)
c
        rand48=i3*1.52587890625D-05+i2*2.328306D-10

      IF(i1.GE.32768) i1=i1-65536
      seed(1)=i1
      IF(i2.GE.32768) i2=i2-65536
      seed(2)=i2
      IF(i3.GE.32768) i3=i3-65536
      seed(3)=i3

      RETURN
      END



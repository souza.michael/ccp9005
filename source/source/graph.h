#include <cmath>
#include <stdexcept>

typedef struct {
	int i;
	int j;
} edge_t;

void edge_show(edge_t* edge) {
	printf("(%d,%d)\n", edge->i, edge->j);
}

int edge_compare(edge_t* a, edge_t* b) {
	if ((a->i == b->i) && (a->j == b->j))
		return 0;
	if ((a->i < b->i) || (a->i == b->i && a->j <= b->j))
		return -1;
	return 1;
}

typedef struct {
	int i; // id
	int d; // dimension
	double x[3]; // location
} node_t;

int node_compare(node_t * a, node_t * b) {
	for (int k = 0; k < a->d; ++k)
		if (a->x[k] < b->x[k])
			return -1;
	return 1;
}


void node_add(node_t * self, const node_t * a)
{
	for (int i = 0; i < a->d; i++)
		self->x[i] += a->x[i];
}


void node_scale(node_t * self, double alpha)
{
	for (int i = 0; i < self->d; ++i)
		self->x[i] *= alpha;
}

void node_show(node_t * self)
{
	printf("%d : ( ", self->i);
	for (int i = 0; i < self->d; i++)
		printf("%g ", self->x[i]);
	printf(")\n");
}

double node_dist(const node_t * a, const node_t * b)
{
	double dst = 0.0, z;
	for (int i = 0; i < a->d; i++)
	{
		z = (a->x[i] - b->x[i]);
		dst += z * z;
	}
	return sqrt(dst);
}


typedef struct
{
	int* i;
	int* j;
	int nrows;
	int ncols;
}csr_t;

// (i,j) in coo format
void csr_create(csr_t * self, int nedges, edge_t * edges)
{
	self->ncols = 0;

	// check if i and j are sorted and if there are not duplicated edges
	for (int k = 1; k < nedges; ++k)
	{
		edge_t* a = &edges[k - 1];
		edge_t* b = &edges[k];

		if (self->ncols < a->j)
			self->ncols = a->j;
		if (self->ncols < b->j)
			self->ncols = b->j;

		if ((a->i > b->i) || ((a->i == b->i) && (a->j >= b->j)))
			throw std::runtime_error("The (i,j) pairs are not sorted.");
	}

	// last row id plus one
	self->nrows = edges[nedges - 1].i + 1;

	// counting nonzero per row
	self->i = (int*)malloc((self->nrows + 1) * sizeof(int));
	for (int k = 0; k < (self->nrows + 1); ++k)
		self->i[k] = 0;
	for (int k = 0; k < nedges; ++k)
		self->i[edges[k].i + 1]++;
	for (int k = 1; k < (self->nrows + 1); ++k)
		self->i[k] += self->i[k - 1]; // cumsum

	// set m_j
	self->j = (int*)malloc(nedges * sizeof(int));
	for (int k = 0; k < nedges; ++k)
		self->j[k] = edges[k].j;
}

void csr_free(csr_t * m) {
	free(m->i);
	free(m->j);
	m->i = NULL;
	m->j = NULL;
	m->ncols = 0;
	m->nrows = 0;
}

inline int csr_neighs(csr_t * m, int i, int** s)
{
	(*s) = &m->j[m->i[i]];
	return m->i[i + 1] - m->i[i];
}

void csr_show(csr_t * self) {
	for (int i = 0; i < self->nrows; ++i)
	{
		printf("[%d: %d] ", i, self->i[i + 1] - self->i[i]);
		for (int k = self->i[i]; k < self->i[i + 1]; k++)
			printf("%d ", self->j[k]);
		printf("\n");
	}
}

void csr_write(csr_t * self, const char* fname) {
	FILE* fid = fopen(fname, "w");
	for (int i = 0; i < self->nrows; ++i)
	{
		fprintf(fid, "[%d: %d] ", i, self->i[i + 1] - self->i[i]);
		for (int k = self->i[i]; k < self->i[i + 1]; k++)
			fprintf(fid, "%d ", self->j[k]);
		fprintf(fid, "\n");
	}
}

typedef struct
{
	int nnodes;
	node_t* nodes;
	int nedges;
	edge_t* edges;
	csr_t m;
}graph_t;

void graph_free(graph_t * g)
{
	free(g->nodes);
	free(g->edges);
	csr_free(&(g->m));

	g->nnodes = g->nedges = 0;
	g->nodes = NULL;
	g->edges = NULL;
}

void graph_create(graph_t * self, int nnodes, node_t * nodes, int nedges, edge_t * edges) {
	self->nnodes = nnodes;
	self->nedges = nedges;
	self->nodes = nodes;
	self->edges = edges;
	csr_create(&(self->m), nedges, edges);
}

void graph_show(graph_t * self)
{
	printf("nnodes: %ld\n", self->nnodes);
	for (int k = 0; k < self->nnodes; ++k)
		node_show(&(self->nodes[k]));

	printf("nedges: %ld\n", self->nedges);
	for (int k = 0; k < self->nedges; ++k)
		edge_show(&(self->edges[k]));

	csr_show(&(self->m));
}

inline int graph_neighs(graph_t * g, int i, int** s)
{
	return csr_neighs(&(g->m), i, s);
}

// Returns the number of clusters
int graph_clusters(graph_t * self, int* c)
{
	const int nnodes = (int)(self->m.nrows);

	for (int i = 0; i < nnodes; ++i)
		c[i] = -1;

	int* s, s_size; // neighs

	// nodes to be visited
	int *v = (int*)malloc(sizeof(int) * nnodes);
	int c_size = 0; // cluster id
	for (int i = 0; i < nnodes; ++i)
	{
		if (c[i] > -1)
			continue;

		v[0] = i;
		for (int k = 1; k > 0;)
		{
			const int j = v[--k];
			c[j] = c_size;

			// add j neighs
			s_size = graph_neighs(self, j, &s);
			for (int q = 0; q < s_size; ++q)
				if (c[s[q]] == -1)
					v[k++] = s[q];
		}
		c_size++;
	}
	free(v);
	return c_size;
}

void graph_write(graph_t * self, const char* prefix)
{
	char name[256];

	{ // write nodes
		sprintf(name, "%s_nodes.csv", prefix);
		printf("Writing file %s\n", name);
		FILE* fid = fopen(name, "w");
		int d = self->nodes[0].d;
		fprintf(fid, "i, ");
		for (int i = 0; i < d; ++i)
			fprintf(fid, "x%ld%s", i, i < (d - 1) ? ", " : "");
		fprintf(fid, "\n");

		for (int k = 0; k < self->nnodes; ++k)
		{
			node_t* a = &(self->nodes[k]);
			fprintf(fid, "%d, ", a->i);
			for (int i = 0; i < a->d; ++i)
				fprintf(fid, "%g%s", a->x[i], i < (a->d - 1) ? ", " : "");
			fprintf(fid, "\n");
		}
		fclose(fid);
	}

	{ // write edges
		sprintf(name, "%s_edges.csv", prefix);
		printf("Writing file %s\n", name);
		FILE* fid = fopen(name, "w");
		fprintf(fid, "i, j\n");
		for (int k = 0; k < self->nedges; ++k)
		{
			edge_t* edge = &(self->edges[k]);
			fprintf(fid, "%d, %d\n", edge->i, edge->j);
		}
		fclose(fid);
	}

	{// write csr
		sprintf(name, "%s_csr.csv", prefix);
		csr_write(&(self->m), name);
	}
}


node_t graph_line(graph_t * self, int nnodes)
{
	int imin = -nnodes / 2;
	int imax = nnodes / 2;
	int root_id = -imin; // this is a positive number

	nnodes = imax - imin + 1;
	node_t* nodes = (node_t*)malloc(sizeof(node_t) * nnodes);
	for (int i = imin; i < (imax + 1); ++i)
	{
		nodes[i - imin].d = 1;
		nodes[i - imin].i = i - imin;
		nodes[i - imin].x[0] = (double)i;
	}

	int nedges = 2 * nnodes - 2;
	edge_t* edges = (edge_t*)malloc(sizeof(edge_t) * nedges);
	int i = 0;
	for (int k = 1; k < nnodes; ++k)
	{
		edges[i].i = nodes[k - 1].i;
		edges[i++].j = nodes[k].i;
		edges[i].i = nodes[k].i;
		edges[i++].j = nodes[k - 1].i;
	}

	graph_create(self, nnodes, nodes, nedges, edges);

	return nodes[root_id];
}

int int_compare(const void* a, const void* b) {
	int int_a = *((int*)a);
	int int_b = *((int*)b);

	if (int_a == int_b) return 0;
	else if (int_a < int_b) return -1;
	else return 1;
}

// Grid size is L x L and p is the probability that node i-th will be inserted
void graph_grid_2D(graph_t * self, const int L, const double p, unsigned int seed)
{
	const int n = L * L; // number of possible nodes
	int* s = (int*)malloc(sizeof(int) * n);

	srand(seed);
	int nnodes = 0;

	// todo: too expensive. could it be reduced?
	for (int i = 0; i < n; ++i)
	{
		if ((((double)rand()) / RAND_MAX) <= p);
		s[nnodes++] = i;
	}

	// begining of each grid row (similar to the csr format)
	int* w = (int*)calloc((nnodes + 1), sizeof(int));; // grid
	for (int k = 0; k < nnodes; ++k)
		if (s[k]) w[k / L + 1]++;

	// w = cumsum(w)
	for (int i = 0; i < n; ++i)
		w[i + 1] += w[i];

	// create nodes and count edges
	int nedges = 0;
	node_t * nodes = (node_t*)malloc(sizeof(node_t) * nnodes);
	for (int k = 0; k < nnodes; ++k)
	{
		const int ai = s[k] / L;
		const int aj = s[k] % L;

		nodes[k].i = k;
		nodes[k].d = 2;
		nodes[k].x[0] = ai;
		nodes[k].x[1] = aj;

		// vertical edge (same column)
		if (k > 0 && ai > 0)
			for (int j = w[ai - 1]; j < w[ai]; ++j)
			{
				const int bj = nodes[j].x[1];
				if (aj == bj)
				{
					nedges += 2;
					break;
				}
			}

		// horizontal edge (same row)
		if (k > 0 && aj > 0)
		{
			const int bi = nodes[k - 1].x[0];
			const int bj = nodes[k - 1].x[1];
			if (ai == bi && aj == (bj + 1))
				nedges += 2;
		}
	}
	free(s);

	// create edges
	edge_t* edges = (edge_t*)malloc(sizeof(edge_t) * nedges);
	nedges = 0;
	for (int k = 0; k < nnodes; ++k)
	{
		const int ai = nodes[k].x[0];
		const int aj = nodes[k].x[1];

		// vertical edge (same column)
		if (k > 0 && ai > 0)
			for (int j = w[ai - 1]; j < w[ai]; ++j)
			{
				const int bj = nodes[j].x[1];
				if (aj == bj)
				{
					edges[nedges].i = nodes[k].i;
					edges[nedges++].j = nodes[j].i;
					edges[nedges].i = nodes[j].i;
					edges[nedges++].j = nodes[k].i;
					break;
				}
			}

		// horizontal edge (same row)
		if (k > 0 && aj > 0)
		{
			const int bi = nodes[k - 1].x[0];
			const int bj = nodes[k - 1].x[1];
			if (ai == bi && aj == (bj + 1))
			{
				edges[nedges].i = nodes[k].i;
				edges[nedges++].j = nodes[k - 1].i;
				edges[nedges].i = nodes[k - 1].i;
				edges[nedges++].j = nodes[k].i;
			}
		}
	}
	free(w);
	graph_create(self, nnodes, nodes, nedges, edges);
}

inline int graph_move_uniform(graph_t * self, int i)
{
	int* s;
	int nz = graph_neighs(self, i, &s);
	return s[rand() % nz];
}
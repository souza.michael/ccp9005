import networkx as nx
import matplotlib.pyplot as plt
import numpy as np

fnodes = 'perc_nodes.csv'
fedges = 'perc_edges.csv'
fclusters = 'perc_clusters.csv'

print('Reading nodes file %s' % fnodes)
pos = {}
with open(fnodes, 'r') as fid:
    data = fid.readlines()
    for row in data:
        if 'id' not in row:
            tokens = row.split(',')
            if len(tokens) > 0:
                id = int(tokens[0])
                x = []
                for i in range(1, len(tokens)):
                    x.append(float(tokens[i]))
                if(len(x) == 1):
                    pos[id] = [x[0], 0]
                elif(len(x) == 2):
                    pos[id] = x
                else:
                    raise Exception('Unsupported dimension.')

g = nx.DiGraph()
for id in pos.keys():
    g.add_node(id)

print('Reading edges file %s' % fedges)
with open(fedges, 'r') as fid:
    data = fid.readlines()
    for row in data:
        if 'i' not in row:
            tokens = row.split(',')
            if len(tokens) > 0:
                i = int(tokens[0])
                j = int(tokens[1])
                g.add_edge(i, j)


print('Reading clusters %s' % fclusters)

clusters = np.ones(len(pos.keys()))
with open(fclusters, 'r') as fid:
    data = fid.readlines()
    for k in range(len(data)):
         clusters[k] = int(data[k])
gc_id = 0 
gc_size = 0 # giant cluster
for k in range(np.max(clusters) + 1):
	c_size = np.sum(clusters == k)
	if c_size > gc_size:
		gc_size = c_size
		gc_id = k

# nx.draw(g, p, with_labels=True)
# nx.draw(g, p, with_labels=False, node_size=30)
nx.draw_networkx_nodes(g, pos, with_labels=False, node_size=30, node_colors=clusters, cmap=plt.cm.jet)
plt.plot()
plt.show()

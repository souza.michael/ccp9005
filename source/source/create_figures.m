x = dlmread('walk.dat');
t = 1:size(x,2); t = t - mean(t); % steps
y = x(:,end);                     % last states
mu = mean(y); sigma = std(y);

fprintf('mu = %f, std = %f\n', mu, sigma);

close all
[p, edges] = histcounts(y, 'Normalization', 'pdf'); hold on
s = (edges(2:end) + edges(1:end-1)) / 2;
z = normpdf(s, 0, sqrt(length(t)));
plot(s, p, 'o');
plot(s, z, '-', 'LineWidth', 1);
legend('data','N(0,t^{1/2})');
xlabel('m (final state)')
ylabel('pdf(m)')


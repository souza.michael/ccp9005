close all

mass = readtable('perc_mass.csv');
figure;
plot(mass.p, mass.mass, '.');
xlabel('p')
ylabel('P_L(L, p)')

nodes = readtable('perc_nodes.csv');
nodes.id = nodes.id + 1;

edges = readtable('perc_edges.csv');
edges.i = edges.i + 1;
edges.j = edges.j + 1;

clusters = readtable('perc_clusters.csv');
clusters.id = clusters.id + 1;

nnodes = length(nodes.id)
nedges = length(edges.i)

x = nodes.x0;
y = nodes.x1;

C = unique(clusters.id);

FIG = figure;
PLOT1 = subplot(1,2,1,'Parent',FIG);
hold(PLOT1, 'on')
for k = 1:length(C)
    cid = clusters.id == C(k);
    plot(x(cid),y(cid),'.','MarkerSize',10);
end

PLOT2 = subplot(1,2,2,'Parent',FIG);
gc_id = 0;   % giant cluster id
gc_len = 0;  % giant cluster length
for k = 1:length(C)
    c_len = sum(clusters.id == C(k));
    if c_len > gc_len
        gc_len = c_len;
        gc_id  = C(k);
    end
end
cid = clusters.id == gc_id;
plot(x(cid),y(cid),'.','MarkerSize',10);
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <omp.h>
#include "graph.h"

void write_ivec(int n, int* v, const char* name) {
	printf("Writing file %s\n", name);
	FILE* fid = fopen(name, "w");
	if (fid == NULL)
		throw std::runtime_error("The file could not be created.");
	fprintf(fid, "i\n");
	for (int k = 0; k < n; ++k)
		fprintf(fid, "%d\n", v[k]);

	fclose(fid);
}

// n is the number of clusters
bool percolates(int L, graph_t * g, int n, int* c)
{
	const int XMIN = 0;
	const int XMAX = L - 1;

	// number of clusters
	double xmin[n];
	double xmax[n];
	double ymin[n];
	double ymax[n];

	for (int i = 0; i < n; ++i)
	{
		xmin[i] = ymin[i] = INT64_MAX;
		xmax[i] = ymax[i] = INT64_MIN;
	}

	for (int i = 0; i < n; ++i)
	{
		double x = g->nodes[i].x[0];
		double y = g->nodes[i].x[1];

		if (xmin[c[i]] > x)
			xmin[c[i]] = x;
		if (xmax[c[i]] < x)
			xmax[c[i]] = x;

		if (ymin[c[i]] > y)
			ymin[c[i]] = y;
		if (ymax[c[i]] < y)
			ymax[c[i]] = y;

		if ((xmin[c[i]] == XMIN && xmax[c[i]] == XMAX) ||
			(ymin[c[i]] == XMIN && ymax[c[i]] == XMAX))
			return true;
	}

	return false;
}

void display(std::vector<int> x, std::string name)
{
	printf("%s = [ ", name.c_str());
	for (int i = 0; i < x.size(); ++i)
		printf("%d ", x[i]);
	printf("\n");
}

double get_pc(int L)
{
	int* c;
	int nc;

	// find pc
	double a = 0, b = 1, p;
	double eps = 1.0 / (L * L);

	double tic, toc_percolates = 0, toc_clusters = 0, toc_grid = 0, toc_graph = 0;

	graph_t g;
	int k = 0;
	while (b - a > eps)
	{
		p = (a + b) / 2;
		printf("[%2d] p = %.7f ", ++k, p);
		tic = omp_get_wtime();
		// create graph
		graph_grid_2D(&g, L, p, 0);
		// g.show();
		toc_graph += omp_get_wtime() - tic;
		printf(" toc %.3f secs\n", omp_get_wtime() - tic);

		tic = omp_get_wtime();
		// g.write("perc");
		int n = graph_clusters(&g, c);
		toc_clusters += omp_get_wtime() - tic;

		tic = omp_get_wtime();
		if (percolates(L, &g, n, c))
			b = p;
		else
			a = p;
		toc_percolates += omp_get_wtime() - tic;
	}
	printf("pc = %.7g \u00b1 %.1g\n", p, b - a);
	printf("Timers (seconds)\n");
	printf("   graph      %g\n", toc_graph);
	printf("   clusters   %g\n", toc_clusters);
	printf("   percolates %g\n", toc_percolates);

	return p;
}

// Return the biggest mass
int get_mass(int c_size, int *c, int *mass)
{
	int max_mass = 0;
	for (int i = 0; i < c_size; ++i)
	{
		mass[c[i]]++;
	}

	for (int i = 0; i < c_size; ++i)
	{
		if (max_mass < mass[i])
			max_mass = mass[i];
	}

	return max_mass;
}

void write_mass(int L, int nsample)
{
	int kmax = 2;
	double* p = (double*)malloc(sizeof(double) * kmax);
	int k = 0;
	for (double pk = 0.3; pk < 1;)
	{
		if (k >= kmax)
		{
			kmax *= 2;
			p = (double*)realloc(p, kmax * sizeof(double));
		}
		p[k++] = pk;
		if (pk < 0.5 || pk >= 0.6)
			pk += 0.01;
		else if (pk < 0.6)
			pk += 0.001;
	}
	int p_size = k;
	printf("write_mass(%d, %d)\n", L, nsample);
	printf("   p_size = %ld\n", p_size);

	double *max_mass = (double*)malloc(sizeof(double) * p_size);
	int *mass = NULL; // mass of each cluster

	int* c = NULL, c_size;
	graph_t g;

	// #pragma omp parallel for
	for (int k = 0; k < p_size; ++k)
	{
		double total_max_mass = 0;
		for (int seed = 0; seed < nsample; ++seed)
		{
			graph_grid_2D(&g, L, p[k], seed);
			c = (int*)malloc(sizeof(int) * g.nnodes);
			c_size = graph_clusters(&g, c);
			mass = (int*)malloc(sizeof(int) * c_size);
			total_max_mass += get_mass(c_size, c, mass);
			free(mass);
			free(c);
		}
		max_mass[k] = total_max_mass / nsample;
	}

	char fname[256];
	sprintf(fname, "perc_mass_L%ld_N%ld.csv", L, nsample);
	printf("Writing %s\n", fname);
	FILE* fid = fopen(fname, "w");
	fprintf(fid, "p, mass\n");
	for (int k = 0; k < p_size; ++k)
		fprintf(fid, "%g, %g\n", p[k], max_mass[k]);
	fclose(fid);
	free(max_mass);
}

int main(int argc, char* argv[])
{
	// calculates critical p
	{
		// const int L = 100;
		// p = get_pc(L); // pc = 0.5927591 ± 1e-06
	}

	// calculates giant cluster
	{
		// const int L = 100;
		// std::vector<node_t> nodes;
		// std::vector<edge_t> edges;
		// std::vector<int> c;
		// double p = 0.5927460;
		// printf("Create edges and nodes with p = %g\n", p);
		// graph_grid_2D(L, p, nodes, edges, 0);
		// graph_t g(nodes, edges);

		// g.write("perc");
		// int nc;
		// clusters(g, nc, c, "perc");
	}

	// write mass statistics
	{
		int L_size = 8;
		int L[] = { 32, 64, 128, 256, 512, 1024, 2048, 4096 };
		int nsample[] = { 10000, 5000, 5000, 500, 500, 200, 200, 100 };
		// std::vector<int> L({32, 64});
		// std::vector<int> nsample({10, 20});
		for (int i = 0; i < L_size; i++)
			write_mass(L[i], nsample[i]);
	}

	return EXIT_SUCCESS;
}

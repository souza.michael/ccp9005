#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <climits>
#include <stdexcept>
#include <omp.h>
#include <string>
#include "graph.h"

void write_vec(const char *fn, std::vector<int> &v)
{
    FILE *fid = fopen(fn, "w");
    printf("Writing file %s\n", fn);
    for (size_t i = 0; i < v.size(); ++i)
        fprintf(fid, "%d ", v[i]);
    fprintf(fid, "\n");
    fclose(fid);
}

void write_vec(const char *fn, std::vector<double> &v)
{
    FILE *fid = fopen(fn, "w");
    printf("Writing file %s\n", fn);
    for (size_t i = 0; i < v.size(); ++i)
        fprintf(fid, "%e ", v[i]);
    fprintf(fid, "\n");
    fclose(fid);
}

inline void swap(int &i, int &j)
{
    const int t = i;
    i = j;
    j = t;
}

inline void swap(double &i, double &j)
{
    const double t = i;
    i = j;
    j = t;
}

class heap_t
{
public:
    heap_t(int capacity) : m_data_i(capacity), m_data_p(capacity)
    {
        m_capacity = capacity;
        m_size = 0;
        m_p = &m_data_p[0];
        m_i = &m_data_i[0];
    }

    int parent(int i) { return (i - 1) / 2; }

    // to get index of left child of node at index i
    int left(int i) { return (2 * i + 1); }

    // to get index of right child of node at index i
    int right(int i) { return (2 * i + 2); }

    // A method to heapify a subtree with the root at given index
    // This method assumes that the subtrees are already heapified
    void heapify(int i)
    {
        int l, r, smallest = i;
        bool done = false;
        while (!done)
        {
            done = true;
            l = left(i);
            r = right(i);
            if (l < m_size && m_p[l] < m_p[i])
                smallest = l;
            if (r < m_size && m_p[r] < m_p[smallest])
                smallest = r;
            if (smallest != i)
            {
                swap(m_p[i], m_p[smallest]);
                swap(m_i[i], m_i[smallest]);
                i = smallest;
                done = false;
            }
        }
    }

    // to extract the root which is the minimum element
    int pop()
    {
        if (m_size <= 0)
            std::runtime_error("The heap is empty.");

        if (m_size == 1)
        {
            m_size--;
            return m_i[0];
        }

        // Store the minimum value, and remove it from heap
        int root = m_i[0];
        m_i[0] = m_i[m_size - 1];
        m_p[0] = m_p[m_size - 1];
        m_size--;

        heapify(0);

        return root;
    }

    // Push a new node (k, p)
    void push(int k, double p)
    {
        if (m_size == m_capacity)
            std::runtime_error("Overflow: Could not insertKey.");

        // First insert the new key at the end
        m_size++;
        int i = m_size - 1;
        m_i[i] = k;
        m_p[i] = p;

        // Fix the min heap property if it is violated
        while (i != 0 && m_p[parent(i)] > m_p[i])
        {
            swap(m_i[i], m_i[parent(i)]);
            swap(m_p[i], m_p[parent(i)]);
            i = parent(i);
        }
    }

    void print()
    {
        for (int i = 0; i < m_size; ++i)
            printf("(%d, %e) ", m_i[i], m_p[i]);
        printf("\n");
    }

    int *m_i;    // node index
    double *m_p; // node invasion resistency
    std::vector<int> m_data_i;
    std::vector<double> m_data_p;
    int m_capacity; // maximum possible size of min heap
    int m_size;     // Current number of elements in min heap
};

// No Trap Invasive Percolation
int apply_ntip(graph_t *g, std::vector<int> &injc, std::vector<int> &sink,
               const std::vector<double> &p, std::vector<int> &iord, int &mass,
               std::vector<bool> &visit)
{
    double tic = omp_get_wtime();

    // sorted to speed up the percolation check
    std::sort(sink.begin(), sink.end());

    // graph center id
    int *I = &injc[0];
    int *S = &sink[0];
    const size_t injc_size = injc.size();
    const size_t sink_size = sink.size();
    std::vector<bool> added(g->nnodes, false); // already in heap
    // invasive percolation
    {
        heap_t h(g->nnodes); // heap
        for (size_t i = 0; i < injc_size; ++i)
        {
            const int j = I[i];
            h.push(j, p[j]); // set injection
            added[j] = true;
        }
        int *V, m, u; // V:neigh and m:len(V)
        mass = 0;     // mass and also time
        while (true)
        {
            u = h.pop();

            // check percolation
            if (std::binary_search(S, S + sink_size, u))
                break;

            iord[mass++] = u;
            visit[u] = true;
            m = graph_neighs(g, u, &V);
            for (int k = 0; k < m; ++k)
            {
                const int j = V[k];
                if (added[j] == false)
                {
                    h.push(j, p[j]);
                    added[j] = true;
                }
            }
        }
    }
    printf("time NTIP        %3.2f secs\n", omp_get_wtime() - tic);
}

void cluster_create(const int UNKW, std::vector<int> &c, int i)
{
    if (c[i] == UNKW)
        c[i] = -1;
}

int cluster_find(const int UNKW, std::vector<int> &c, int i)
{
    if (c[i] == UNKW)
        return UNKW;

    int r, s;
    r = s = i;
    while (c[r] >= 0)
    {
        c[s] = c[r];
        s = r;
        r = c[r];
    }
    return r;
}

void cluster_merge(const int UNKW, std::vector<int> &c, int i, int j)
{
    if (c[i] == UNKW)
        cluster_create(UNKW, c, i);
    if (c[j] == UNKW)
        cluster_create(UNKW, c, j);

    i = cluster_find(UNKW, c, i);
    j = cluster_find(UNKW, c, j);

    if (i == j)
        return;

    if (-c[i] > -c[j])
    {
        c[i] += c[j];
        c[j] = i;
    }
    else
    {
        c[j] += c[i];
        c[i] = j;
    }
}

void cluster_show(const int UNKW, std::vector<int> &c)
{
    bool newline;
    printf("\n");
    for (int k = 0; k < (int)c.size(); ++k)
    {
        newline = false;
        for (int i = 0; i < c.size(); ++i)
        {
            if (cluster_find(UNKW, c, i) == k)
            {
                printf("%d ", i);
                newline = true;
            }
        }
        if (newline)
            printf("\n");
    }
}

// Trap Invasive Percolation
int apply_tip(graph_t *g, std::vector<int> &sink,
              const std::vector<int> &iord_ntip, int mass_ntip, const std::vector<bool> &vs_ntip,
              std::vector<int> &iord_tip, int &mass_tip)
{
    double tic = omp_get_wtime();

    const int UNKW = g->nnodes;
    std::vector<int> c(g->nnodes, UNKW);

    int *S = &sink[0];
    int sink_size = (int)sink.size();

    // std::sort(S, S + sink_size);

    // merge non-visited clusters (defending fluid)
    int m, *V;
    for (int i = 0; i < g->nnodes; ++i)
    {
        if (!vs_ntip[i])
        {
            m = graph_neighs(g, i, &V);
            cluster_create(UNKW, c, i);
            for (int k = 0; k < m; ++k)
            {
                const int j = V[k];
                if (!vs_ntip[j])
                    cluster_merge(UNKW, c, i, j);
            }
        }
    }

    // merge sink clusters
    int s = sink[0];
    for (size_t i = 0; i < sink.size(); ++i)
        cluster_merge(UNKW, c, s, sink[i]);

    // un-invade all sites one by one in reverse order
    mass_tip = 0; // total invaded sites during tip
    for (int i = (mass_ntip - 1); i >= 0; --i)
    {
        const int u = iord_ntip[i];
        if (vs_ntip[u])
        {
            cluster_create(UNKW, c, u);
            m = graph_neighs(g, u, &V);
            for (int j = 0; j < m; ++j)
                if (cluster_find(UNKW, c, V[j]) != UNKW)
                    cluster_merge(UNKW, c, u, V[j]);

            if (cluster_find(UNKW, c, u) == cluster_find(UNKW, c, s))
                iord_tip[mass_tip++] = u;
        }
    }

    // reverse tm_tip
    std::reverse(&iord_tip[0], &iord_tip[0] + mass_tip);

    printf("time TIP         %3.2f secs\n", omp_get_wtime() - tic);
}

int main(int argc, char *argv[])
{
    char fn[256];
    const int K = 1024; // sample size
    const int L = atoi(argv[1]);
    const int N = L * L;

    double tic;
    tic = omp_get_wtime();
    // creates 2D graph
    graph_t g;
    graph_grid_2D(&g, L, 1, false, true, 0);
    printf("time creation    %3.2f secs\n", omp_get_wtime() - tic);

    sprintf(fn, "graph_L%d", L);
    graph_write(&g, fn);

    // set injection (center)
    std::vector<int> injc(0);
    {
        auto select = [L](node_t &u) {
            return u.x[0] == (L - 1);
        };
        graph_select(&g, select, injc);
    }

    // set sink (boundaries)
    std::vector<int> sink(0);
    {
        auto select = [L](node_t &u) {
            return u.x[0] == 0;
        };
        graph_select(&g, select, sink);
    }

    // mass
    std::vector<int> mass_ntip(K);
    std::vector<int> mass_tip(K);
    double total_mass_tip = 0;
    double total_mass_ntip = 0;

    // random invasion potential
    std::vector<double> p(N);

    // main loop
    for (int i = 0; i < K; ++i)
    {
        double tic = omp_get_wtime();
        std::vector<int> iord_ntip(N, -1);     // invasion order in ntip
        std::vector<int> iord_tip(N, -1);      // invasion order in tip
        std::vector<bool> vist_ntip(N, false); // vs_ntip[i] == true iff the site i has been visited=
        for (int k = 0; k < N; ++k)
            p[k] = ((double)rand()) / RAND_MAX;

        apply_ntip(&g, injc, sink, p, iord_ntip, mass_ntip[i], vist_ntip);
        apply_tip(&g, sink, iord_ntip, mass_ntip[i], vist_ntip, iord_tip, mass_tip[i]);

        // write_vec("vec_iord_ntip.csv", iord_ntip);
        // write_vec("vec_iord_tip.csv", iord_tip);
        // write_vec("vec_p.csv", p);
        printf("[%d] time inv_perc %3.2f secs\n", i, omp_get_wtime() - tic);
    }

    sprintf(fn, "vec_mass_tip_L%d.csv", L);
    write_vec(fn, mass_tip);

    sprintf(fn, "vec_mass_ntip_L%d.csv", L);
    write_vec(fn, mass_ntip);

    return EXIT_SUCCESS;
}
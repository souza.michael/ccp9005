close all
figure; hold on
data = dlmread('fire_L128_N2.dat');
c = data(:,1); % cluster size
p = data(:,2); % fraction of percolating clusters
c = c / max(c);
x = (1:length(c)) / length(c);
subplot(1,2,1); hold on
plot(x, c,'DisplayName','L=128')
subplot(1,2,2); hold on
plot(x, p,'DisplayName','L=128')
data = dlmread('fire_L512_N2.dat');
c = data(:,1); % cluster size
p = data(:,2); % fraction of percolating clusters
c = c / max(c);
x = (1:length(c)) / length(c);
subplot(1,2,1); hold on
plot(x, c,'DisplayName','L=512')
subplot(1,2,2); hold on
plot(x, p,'DisplayName','L=512')
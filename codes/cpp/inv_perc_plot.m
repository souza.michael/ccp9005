S = [32 64 128 256 512 1024];
A = zeros(size(S));
B = zeros(size(S));

for k = 1:length(S)
    mass_tip = dlmread(sprintf('vec_mass_tip_L%d.csv', S(k)));
    mass_ntip = dlmread(sprintf('vec_mass_ntip_L%d.csv', S(k)));
    
    A(k) = mean(mass_tip);
    B(k) = mean(mass_ntip);
    
%     close all
%     subplot(1,2,1)
%     histogram(mass_tip, 30)
%     title('MASS TIP')
%     subplot(1,2,2)
%     histogram(mass_ntip, 30)
%     title('MASS NTIP')
end

close all

subplot(1,2,1)
loglog(S, A, '*');
title('TIP')

subplot(1,2,2)
loglog(S, B, '*');
title('NTIP')

dlmwrite('vec_grace.dat',[S', A', B'], 'Delimiter', ' ')
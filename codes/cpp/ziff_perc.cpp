#include <cstdlib>
#include <cstdio>

#define L 512 // linear dimension
#define N (L * L)
#define EMPTY (-N - 1)

int ptr[N];   // array of pointers
int nn[N][4]; // nearest neighbors
int order[N]; // occupation order

// set periodic boundaries
void boundaries()
{
    for (int i = 0; i < N; i++)
    {
        nn[i][0] = (i + 1) % N;
        nn[i][1] = (i + N - 1) % N;
        nn[i][2] = (i + L) % N;
        nn[i][3] = (i + N - L) % N;
        if (i % L == 0)
            nn[i][1] = i + L - 1;
        if ((i + 1) % L == 0)
            nn[i][0] = i - L + 1;
    }
}

void permutation()
{
    for (int i = 0; i < N; ++i)
        order[i] = i;
    for (int i = 0, j, temp; i < N; i++)
    {
        j = i + (N - i - 1) * ((double)rand()) / RAND_MAX;
        temp = order[i];
        order[i] = order[j];
        order[j] = temp;
    }
}

int findroot(int i)
{
    int r, s;
    r = s = i;
    while (ptr[r] >= 0)
    {
        ptr[s] = ptr[r];
        s = r;
        r = ptr[r];
    }
    return r;
}

void percolate(double *BIG)
{
    int big = 0;
    for (int i = 0; i < N; i++)
        ptr[i] = EMPTY;
    for (int i = 0; i < N; i++)
    {
        int r1, s1, r2, s2;
        r1 = s1 = order[i];
        ptr[s1] = -1;
        for (int j = 0; j < 4; j++)
        {
            s2 = nn[s1][j];
            if (ptr[s2] != EMPTY)
            {
                r2 = findroot(s2);
                if (r2 != r1)
                {
                    if (ptr[r1] > ptr[r2])
                    {
                        ptr[r2] += ptr[r1];
                        ptr[r1] = r2;
                        r1 = r2;
                    }
                    else
                    {
                        ptr[r1] += ptr[r2];
                        ptr[r2] = r1;
                    }
                    if (-ptr[r1] > big)
                        big = -ptr[r1];
                }
            }
        }
        BIG[i] += big;
    }
}

int main(int argc, char *argv[])
{
    boundaries();
    double BIG[N]; // average biggest cluster size
    int nsample = std::atoi(argv[1]);

    for (int i = 0; i < N; ++i)
        BIG[i] = 0;

    for (int i = 0; i < nsample; i++)
    {
        permutation();
        percolate(BIG);
    }

    for (int i = 0; i < N; ++i)
        BIG[i] /= nsample;

    for (int i = 0; i < N; ++i)
        printf("%g ", BIG[i]);
    printf("\n");
    return EXIT_SUCCESS;
}
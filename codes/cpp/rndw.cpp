#include "graph.h"

typedef struct
{
	graph_t* g;
	int* walk;
	node_t* root;
	int nsteps;
	int nwalks;
}rndw_t;

void rndw_create(rndw_t* self, graph_t* g, node_t* root) {
	self->g = g;
	self->root = root;
	self->nsteps = 0;
	self->nwalks = 0;
	self->walk = NULL;
}

void rndw_free(rndw_t* self) {
	free(self->walk);
	self->nsteps = 0;
	self->g = NULL;
	self->root = NULL;
	self->nwalks = 0;
	self->walk = NULL;
}

// executes a single random walk with nsteps
void rndw_walk(rndw_t* self, int nsteps, int move(graph_t*, int), int* w)
{
	w[0] = move(self->g, self->root->i);
	for (int i = 1; i < nsteps; ++i)
		w[i] = move(self->g, w[i - 1]);
}

// executes nwalks walks of nsteps steps each
void rndw_run(rndw_t * self, int nwalks, int nsteps, int move(graph_t*, int))
{
	self->nsteps = nsteps;
	self->nwalks = nwalks;

	if (self->walk) free(self->walk);
	self->walk = (int*)malloc(sizeof(int) * nwalks * nsteps);

	// TODO add OpenMP
	for (int i = 0; i < nwalks; ++i)
		rndw_walk(self, nsteps, move, &(self->walk[i * nsteps]));
}

// mean final states taken over all walks
node_t rndw_mu(rndw_t * self)
{
	int nwalks = self->nwalks;
	int nsteps = self->nsteps;

	// start mu
	node_t mu;
	mu.i = -1;
	mu.d = self->g->nodes[0].d;
	for (int i = 0; i < mu.d; ++i)
		mu.x[i] = 0.0;

	// loop over final states
	for (int k = 0; k < nwalks; ++k)
	{
		int i = self->walk[(k + 1) * nsteps - 1];
		node_add(&mu, &(self->g->nodes[i]));
	}
	node_scale(&mu, 1.0 / nwalks);
	return mu;
}

// vector of distance from root
void rndw_dist(rndw_t * self, double* d)
{
	int nwalks = self->nwalks;
	int nsteps = self->nsteps;

	for (int i = 0; i < nsteps; ++i)
		d[i] = 0.0;

	node_t * root = self->root;
	for (int k = 0; k < nwalks; ++k)
		for (int i = 0; i < nsteps; i++)
		{
			node_t* w = &(self->g->nodes[self->walk[k * nsteps + i]]);
			d[i] += node_dist(root, w) / nwalks;
		}
}

void rndw_show(rndw_t * self)
{
	int nwalks = self->nwalks;
	int nsteps = self->nsteps;
	if (nwalks <= 10)
		for (int i = 0; i < nwalks; i++)
		{
			for (int k = 0; k < nsteps; ++k)
			{
				const int w = self->walk[i * nsteps + k];
				printf("% g ", self->g->nodes[w].x[0]);
			}
			printf("\n");
		}
	else
	{
		for (int i = 0; i < 5; i++)
		{
			for (int k = 0; k < nsteps; ++k)
			{
				const int w = self->walk[i * nsteps + k];
				printf("% g ", self->g->nodes[w].x[0]);
			}
			printf("\n");
		}
		printf("   ...\n");
		for (int i = nwalks - 6; i < nwalks; i++)
		{
			for (int k = 0; k < nsteps; ++k)
			{
				const int w = self->walk[i * nsteps + k];
				printf("% g ", self->g->nodes[w].x[0]);
			}
			printf("\n");
		}
	}
}

int main(int argc, char* argv[])
{
	const int nsteps = 100;
	const int nwalks = 100000;

	graph_t g;
	node_t root = graph_line(&g, nsteps);

	// graph_show(&g);
	graph_write(&g, "rndw");

	rndw_t w;
	rndw_create(&w, &g, &root);

	rndw_run(&w, nwalks, nsteps, graph_move_uniform);
	// rndw_show(&w);

	// mean of final states
	node_t mu = rndw_mu(&w);

	// vector of distance to root
	double r[nsteps];
	rndw_dist(&w, r);

	printf("mu -> ");
	node_show(&mu);

	printf("r = [ ");
	for (int i = 0; i < nsteps; i++)
		printf("%g%s ", r[i], i < (nsteps - 1) ? "," : "");
	printf("]\n");

	return EXIT_SUCCESS;
}
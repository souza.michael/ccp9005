#pragma once
#include <cmath>
#include <cstdlib>
#include <stdexcept>
#include <algorithm>
#include <functional>

#ifdef __linux__
void fopen_s(FILE **fid, const char *name, const char *mode)
{
	*fid = fopen(name, mode);
}
#define sprintf_s sprintf
#endif

typedef struct
{
	int d;			// dimension
	double xmin[3]; // xmin[i] :: min in direction i
	double xmax[3]; // xmax[i] :: max in direction i
} bbox_t;

typedef struct
{
	int i;		 // id
	int d;		 // dimension
	double x[3]; // location
} node_t;

typedef struct
{
	int i;
	int j;
} edge_t;

typedef struct
{
	int *i;
	int *j;
	int nrows;
	int ncols;
} csr_t;

typedef struct
{
	int nnodes;
	node_t *nodes;
	int nedges;
	edge_t *edges;
	csr_t m;
	bbox_t bbox;
} graph_t;

void ivec_write(int n, int *v, const char *name)
{
	printf("Writing file %s\n", name);
	FILE *fid;
	fopen_s(&fid, name, "w");
	if (fid == NULL)
		throw std::runtime_error("The file could not be created.");

	fprintf(fid, "i\n");
	for (int k = 0; k < n; ++k)
		fprintf(fid, "%d\n", v[k]);

	fclose(fid);
}

void dvec_write(int n, double *v, const char *name)
{
	printf("Writing file %s\n", name);
	FILE *fid;
	fopen_s(&fid, name, "w");
	if (fid == NULL)
		throw std::runtime_error("The file could not be created.");

	fprintf(fid, "i\n");
	for (int k = 0; k < n; ++k)
		fprintf(fid, "%g\n", v[k]);

	fclose(fid);
}

void edge_show(edge_t *edge)
{
	printf("(%d,%d)\n", edge->i, edge->j);
}

int edge_compare(const void *a, const void *b)
{
	edge_t *pa = (edge_t *)a;
	edge_t *pb = (edge_t *)b;
	if ((pa->i == pb->i) && (pa->j == pb->j))
		return 0;
	if ((pa->i < pb->i) || (pa->i == pb->i && pa->j <= pb->j))
		return -1;
	return 1;
}

int node_compare(node_t *a, node_t *b)
{
	for (int k = 0; k < a->d; ++k)
		if (a->x[k] < b->x[k])
			return -1;
	return 1;
}

void node_add(node_t *self, const node_t *a)
{
	for (int i = 0; i < a->d; i++)
		self->x[i] += a->x[i];
}

void node_scale(node_t *self, double alpha)
{
	for (int i = 0; i < self->d; ++i)
		self->x[i] *= alpha;
}

void node_show(node_t *self)
{
	printf("%d : ( ", self->i);
	for (int i = 0; i < self->d; i++)
		printf("%g ", self->x[i]);
	printf(")\n");
}

double node_dist(const node_t *a, const node_t *b)
{
	double dst = 0.0, z;
	for (int i = 0; i < a->d; i++)
	{
		z = (a->x[i] - b->x[i]);
		dst += z * z;
	}
	return sqrt(dst);
}

// (i,j) in coo format
void csr_create(csr_t *self, int nnodes, int nedges, edge_t *edges)
{
	self->ncols = self->nrows = nnodes;

	// check if i and j are sorted and if there are duplicated edges
	for (int k = 1; k < nedges; ++k)
	{
		edge_t *a = &edges[k - 1];
		edge_t *b = &edges[k];

		if (self->ncols < a->j)
			self->ncols = a->j;
		if (self->ncols < b->j)
			self->ncols = b->j;

		if ((a->i > b->i) || ((a->i == b->i) && (a->j >= b->j)))
			throw std::runtime_error("The (i,j) pairs are not sorted.");
	}

	// counting nonzero per row
	self->i = (int *)malloc((self->nrows + 1) * sizeof(int));
	for (int k = 0; k < (self->nrows + 1); ++k)
		self->i[k] = 0;
	for (int k = 0; k < nedges; ++k)
		self->i[edges[k].i + 1]++;
	for (int k = 1; k < (self->nrows + 1); ++k)
		self->i[k] += self->i[k - 1]; // cumsum

	// set m_j
	self->j = (int *)malloc(nedges * sizeof(int));
	for (int k = 0; k < nedges; ++k)
		self->j[k] = edges[k].j;
}

void csr_free(csr_t *m)
{
	free(m->i);
	free(m->j);
	m->i = NULL;
	m->j = NULL;
	m->ncols = 0;
	m->nrows = 0;
}

inline int csr_neighs(csr_t *m, int i, int **s)
{
	(*s) = &m->j[m->i[i]];
	return m->i[i + 1] - m->i[i];
}

void csr_show(csr_t *self)
{
	for (int i = 0; i < self->nrows; ++i)
	{
		printf("[%d: %d] ", i, self->i[i + 1] - self->i[i]);
		for (int k = self->i[i]; k < self->i[i + 1]; k++)
			printf("%d ", self->j[k]);
		printf("\n");
	}
}

void csr_write(csr_t *self, const char *fname)
{
	FILE *fid;
	fopen_s(&fid, fname, "w");
	if (fid == NULL)
		throw std::runtime_error("File could not be opened.");

	for (int i = 0; i < self->nrows; ++i)
	{
		fprintf(fid, "[%d: %d] ", i, self->i[i + 1] - self->i[i]);
		for (int k = self->i[i]; k < self->i[i + 1]; k++)
			fprintf(fid, "%d ", self->j[k]);
		fprintf(fid, "\n");
	}

	fclose(fid);
}

void graph_free(graph_t *g)
{
	free(g->nodes);
	free(g->edges);
	csr_free(&(g->m));

	g->nnodes = g->nedges = 0;
	g->nodes = NULL;
	g->edges = NULL;
}

void graph_bbox(graph_t *self, bbox_t *bbox)
{
	node_t node = self->nodes[0];
	bbox->d = self->nodes[0].d;
	bbox->xmin[0] = bbox->xmin[1] = bbox->xmin[2] = +MAXFLOAT;
	bbox->xmax[0] = bbox->xmax[1] = bbox->xmax[2] = -MAXFLOAT;

	for (int i = 0; i < self->nnodes; ++i)
		for (int d = 0; d < node.d; ++d)
		{
			const double xd = self->nodes[i].x[d];
			if (bbox->xmin[d] > xd)
				bbox->xmin[d] = xd;
			if (bbox->xmax[d] < xd)
				bbox->xmin[d] = xd;
		}
}

void graph_create(graph_t *self, int nnodes, node_t *nodes, int nedges, edge_t *edges)
{
	self->nnodes = nnodes;
	self->nedges = nedges;
	self->nodes = nodes;
	self->edges = edges;
	csr_create(&(self->m), nnodes, nedges, edges);
	graph_bbox(self, &self->bbox);
}

int graph_closest(graph_t *self, node_t *v)
{
	int u;
	double dmin = MAXFLOAT;
	for (int i = 0; i < self->nnodes; ++i)
	{
		double d = node_dist(&self->nodes[i], v);
		if (d < dmin)
		{
			dmin = d;
			u = i;
		}
	}
	return u;
}

void graph_show(graph_t *self)
{
	printf("nnodes: %d\n", self->nnodes);
	for (int k = 0; k < self->nnodes; ++k)
		node_show(&(self->nodes[k]));

	printf("nedges: %d\n", self->nedges);
	for (int k = 0; k < self->nedges; ++k)
		edge_show(&(self->edges[k]));

	csr_show(&(self->m));
}

inline int graph_neighs(graph_t *g, int i, int **s)
{
	return csr_neighs(&(g->m), i, s);
}

// Returns the number of clusters
int graph_clusters(graph_t *self, int *c)
{
	const int nnodes = (int)(self->nnodes);

	for (int i = 0; i < nnodes; ++i)
		c[i] = -1;

	int *s, s_size; // neighs

	// nodes to be visited
	int *v = (int *)malloc(sizeof(int) * nnodes);
	int c_size = 0; // cluster id
	for (int i = 0; i < nnodes; ++i)
	{
		if (c[i] > -1)
			continue;

		v[0] = i;
		for (int k = 1; k > 0;)
		{
			const int j = v[--k];
			c[j] = c_size;

			// add j neighs
			s_size = graph_neighs(self, j, &s);
			for (int q = 0; q < s_size; ++q)
				if (c[s[q]] == -1)
					v[k++] = s[q];
		}
		c_size++;
	}
	free(v);
	return c_size;
}

void graph_write(graph_t *self, const char *prefix)
{
	char name[256];

	{ // write nodes
		sprintf_s(name, "%s_nodes.csv", prefix);
		printf("Writing file %s\n", name);
		FILE *fid;
		fopen_s(&fid, name, "w");
		int d = self->nodes[0].d;
		fprintf(fid, "i,");
		if (d > 3)
			throw std::runtime_error("Not supported dimension (d>3).");
		char c[3] = {'x', 'y', 'z'};
		for (int i = 0; i < d; ++i)
			fprintf(fid, "%c%s", c[i], i < (d - 1) ? "," : "");
		fprintf(fid, "\n");

		for (int k = 0; k < self->nnodes; ++k)
		{
			node_t *a = &(self->nodes[k]);
			fprintf(fid, "%d,", a->i);
			for (int i = 0; i < a->d; ++i)
				fprintf(fid, "%g%s", a->x[i], i < (a->d - 1) ? "," : "");
			fprintf(fid, "\n");
		}
		fclose(fid);
	}

	{ // write edges
		sprintf_s(name, "%s_edges.csv", prefix);
		printf("Writing file %s\n", name);
		FILE *fid;
		fopen_s(&fid, name, "w");
		fprintf(fid, "i,j\n");
		for (int k = 0; k < self->nedges; ++k)
		{
			edge_t *edge = &(self->edges[k]);
			fprintf(fid, "%d,%d\n", edge->i, edge->j);
		}
		fclose(fid);
	}

	{ // write csr
		sprintf_s(name, "%s_csr.csv", prefix);
		csr_write(&(self->m), name);
	}
}

node_t graph_line(graph_t *self, int nnodes)
{
	int imin = -nnodes / 2;
	int imax = nnodes / 2;
	int root_id = -imin; // this is a positive number

	nnodes = imax - imin + 1;
	node_t *nodes = (node_t *)malloc(sizeof(node_t) * nnodes);
	for (int i = imin; i < (imax + 1); ++i)
	{
		nodes[i - imin].d = 1;
		nodes[i - imin].i = i - imin;
		nodes[i - imin].x[0] = (double)i;
	}

	int nedges = 2 * nnodes - 2;
	edge_t *edges = (edge_t *)malloc(sizeof(edge_t) * nedges);
	int i = 0;
	for (int k = 1; k < nnodes; ++k)
	{
		edges[i].i = nodes[k - 1].i;
		edges[i++].j = nodes[k].i;
		edges[i].i = nodes[k].i;
		edges[i++].j = nodes[k - 1].i;
	}

	graph_create(self, nnodes, nodes, nedges, edges);

	return nodes[root_id];
}

int int_compare(const void *a, const void *b)
{
	int int_a = *((int *)a);
	int int_b = *((int *)b);

	if (int_a == int_b)
		return 0;
	else if (int_a < int_b)
		return -1;
	else
		return 1;
}

// Creates a L x L 2D Grid, where p is the probability that node i-th will be inserted.
// vert :: indicates if there is vertical periodic boundary condition
// horz :: indicates if there is horizontal periodic boundary condition
void graph_grid_2D(graph_t *self, const int L, const double p, bool vert, bool horz, unsigned int seed)
{
	const int n = L * L; // number of possible nodes
	int *s = (int *)malloc(sizeof(int) * n);

	srand(seed);
	int nnodes = 0;

	// todo: too expensive. could it be reduced?
	for (int i = 0; i < n; ++i)
	{
		const double pk = (((double)rand()) / RAND_MAX);
		if (pk <= p)
			s[nnodes++] = i;
	}

	// begining of each grid row (similar to the csr format)
	int *w = (int *)calloc((nnodes + 1), sizeof(int));
	// grid
	for (int k = 0; k < nnodes; ++k)
	{
		const int i = s[k] / L;
		w[i + 1]++;
	}

	// w = cumsum(w)
	for (int i = 0; i < nnodes; ++i)
		w[i + 1] += w[i];

	// create nodes
	node_t *nodes = (node_t *)malloc(sizeof(node_t) * nnodes);
	for (int k = 0; k < nnodes; ++k)
	{
		const int ai = (int)(s[k] / L); // row
		const int aj = (int)(s[k] % L); // col

		nodes[k].i = k;
		nodes[k].d = 2;
		nodes[k].x[0] = ai;
		nodes[k].x[1] = aj;
	}
	free(s);

	// count edges
	int nedges = 0;
	for (int k = 0; k < nnodes; ++k)
	{
		const int ai = (int)nodes[k].x[0]; // row
		const int aj = (int)nodes[k].x[1]; // col

		// vertical edge (same column)
		if (k > 0 && ai > 0)
		{ // previous row
			for (int j = w[ai - 1]; j < w[ai]; ++j)
			{
				const int bj = (int)nodes[j].x[1];
				if (aj == bj)
				{
					nedges += 2;
					break;
				}
			}
		}

		// horizontal edge (same row)
		if (k > 0 && aj > 0)
		{
			const int bi = (int)nodes[k - 1].x[0];
			const int bj = (int)nodes[k - 1].x[1];
			// previous col
			if (ai == bi && aj == (bj + 1))
				nedges += 2;
		}

		// add vert periodic boundary
		if (vert && ai == 0)
		{
			// last row
			for (int j = w[L - 1]; j < w[L]; ++j)
			{
				const int bj = (int)nodes[j].x[1];
				if (aj == bj)
				{
					nedges += 2;
					break;
				}
			}
		}

		// add horz periodic boundary
		if (horz && aj == 0)
		{
			// last point
			const int j = w[ai + 1] - 1;
			const int bi = (int)nodes[j].x[0];
			const int bj = (int)nodes[j].x[1];
			if (ai == bi && bj == (L - 1))
				nedges += 2;
		}
	}

	// create edges
	edge_t *edges = (edge_t *)malloc(sizeof(edge_t) * nedges);
	nedges = 0;
	for (int k = 0; k < nnodes; ++k)
	{
		const int ai = (int)nodes[k].x[0];
		const int aj = (int)nodes[k].x[1];

		// vertical edge (same column)
		if (k > 0 && ai > 0)
		{ // previous row
			for (int j = w[ai - 1]; j < w[ai]; ++j)
			{
				const int bj = (int)nodes[j].x[1];
				if (aj == bj)
				{
					edges[nedges].i = nodes[k].i;
					edges[nedges++].j = nodes[j].i;
					edges[nedges].i = nodes[j].i;
					edges[nedges++].j = nodes[k].i;
					break;
				}
			}
		}

		// horizontal edge (same row)
		if (k > 0 && aj > 0)
		{
			const int bi = (int)nodes[k - 1].x[0];
			const int bj = (int)nodes[k - 1].x[1];
			// previous col
			if (ai == bi && aj == (bj + 1))
			{
				edges[nedges].i = nodes[k].i;
				edges[nedges++].j = nodes[k - 1].i;
				edges[nedges].i = nodes[k - 1].i;
				edges[nedges++].j = nodes[k].i;
			}
		}

		// add vert periodic boundary
		if (vert && ai == 0)
		{
			// last row
			for (int j = w[L - 1]; j < w[L]; ++j)
			{
				const int bj = (int)nodes[j].x[1];
				if (aj == bj)
				{
					edges[nedges].i = nodes[k].i;
					edges[nedges++].j = nodes[j].i;
					edges[nedges].i = nodes[j].i;
					edges[nedges++].j = nodes[k].i;
					break;
				}
			}
		}

		// add horz periodic boundary
		if (horz && aj == 0)
		{
			// last point
			const int j = w[ai + 1] - 1;
			const int bi = (int)nodes[j].x[0];
			const int bj = (int)nodes[j].x[1];
			if (ai == bi && bj == (L - 1))
			{
				edges[nedges].i = nodes[k].i;
				edges[nedges++].j = nodes[j].i;
				edges[nedges].i = nodes[j].i;
				edges[nedges++].j = nodes[k].i;
			}
		}
	}
	free(w);

	qsort(edges, nedges, sizeof(edge_t), edge_compare);
	graph_create(self, nnodes, nodes, nedges, edges);
}

inline int graph_move_uniform(graph_t *self, int i)
{
	int *s;
	int nz = graph_neighs(self, i, &s);
	return s[rand() % nz];
}

// Returns center id
int graph_center(graph_t *self)
{
	bbox_t bbox;
	graph_bbox(self, &bbox);

	double c[3]; // bbox center
	for (int d = 0; d < bbox.d; ++d)
		c[d] = (bbox.xmin[d] + bbox.xmax[d]) / 2.0;

	int cid; // graph center id

	// find graph point closest to c
	double dmin = MAXFLOAT;
	for (int k = 0; k < self->nnodes; ++k)
	{
		double *x = self->nodes[k].x;
		double d = 0.0;
		for (int d = 0; d < bbox.d; ++d)
			d += (c[d] - x[d]) * (c[d] - x[d]);

		if (dmin > d)
		{
			cid = k;
			dmin = d;
		}
	}

	return cid;
}

int graph_maxdeg(graph_t *self)
{
	int maxdeg = 0;
	for (int i = 0; i < self->nnodes; ++i)
	{
		const int deg = self->m.i[i + 1] - self->m.i[i];
		if (maxdeg < deg)
			maxdeg = deg;
	}
	return maxdeg;
}

// v :: list of nodes u such that select(u) is true
void graph_select(graph_t *self,
				  std::function<bool(node_t &)> select,
				  std::vector<int> &v)
{
	v.clear();
	for (int i = 0; i < self->nnodes; ++i)
		if (select(self->nodes[i]))
			v.push_back(self->nodes[i].i);
}
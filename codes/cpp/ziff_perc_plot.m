close all
figure; hold on
data = dlmread('perc_L128_N2.dat');
data = data / max(data);
x = (1:length(data)) / length(data);
plot(x, data,'DisplayName','L=128')
data = dlmread('perc_L256_N2.dat');
data = data / max(data);
x = (1:length(data)) / length(data);
plot(x, data,'DisplayName','L=256')
data = dlmread('perc_L512_N2.dat');
data = data / max(data);
x = (1:length(data)) / length(data);
plot(x, data,'DisplayName','L=512')
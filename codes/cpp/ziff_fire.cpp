#include <cstdlib>
#include <cstdio>

#define L 512 // linear dimension
#define N (L * L)
#define EMPTY (-N - 1)
#define NORTH 0
#define EAST 1
#define WEST 2
#define SOUTH 3

int ptr[N];      // array of pointers
int nn[N][4][2]; // nearest neighbors
int order[N];    // occupation order

// set periodic boundaries
void boundaries()
{
    for (int i = 0; i < N; i++)
        for (int j = 0; j < 4; j++)
            nn[i][j][0] = 0;

    for (int i = 0; i < N; i++)
    {
        if (i > (L - 1))
        {
            nn[i][NORTH][0] = 1;
            nn[i][NORTH][1] = i - L;
        }
        if (i < L * L - L)
        {
            nn[i][SOUTH][0] = 1;
            nn[i][SOUTH][1] = i + L;
        }
        if (i % L)
        {
            nn[i][WEST][0] = 1;
            nn[i][WEST][1] = i - 1;
        }
        if ((i + 1) % L)
        {
            nn[i][EAST][0] = 1;
            nn[i][EAST][1] = i + 1;
        }
    }
}

void permutation()
{
    for (int i = 0; i < L; ++i)
    {
        order[i] = i;
        order[L + i] = N - L + i;
    }
    for (int i = 2 * L; i < N; i++)
        order[i] = i - L;

    for (int i = 2 * L, j, temp; i < N; i++)
    {
        j = i + (N - i - 1) * ((double)rand()) / RAND_MAX;
        temp = order[i];
        order[i] = order[j];
        order[j] = temp;
    }
}

int findroot(int i)
{
    int r, s;
    r = s = i;
    while (ptr[r] >= 0)
    {
        ptr[s] = ptr[r];
        s = r;
        r = ptr[r];
    }
    return r;
}

void percolate(double *FIRE, double *PERC)
{
    int fire = 0;
    for (int i = 0; i < N; i++)
        ptr[i] = EMPTY;
    for (int i = 0; i < N; i++)
    {
        int r1, s1, r2, s2;
        r1 = s1 = order[i];
        ptr[s1] = -1;
        for (int j = 0; j < 4; j++)
        {
            // check if the neigh is there
            if (!nn[s1][j][0])
                continue;
            s2 = nn[s1][j][1];
            if (ptr[s2] != EMPTY)
            {
                r2 = findroot(s2);
                if (r2 != r1)
                {
                    if (ptr[r1] > ptr[r2])
                    {
                        ptr[r2] += ptr[r1];
                        ptr[r1] = r2;
                        r1 = r2;
                    }
                    else
                    {
                        ptr[r1] += ptr[r2];
                        ptr[r2] = r1;
                    }
                }
            }
        }
        int rl = findroot(0);     // root left (fire)
        int rr = findroot(N - 1); // root right (percolate)
        FIRE[i] += -ptr[rl];
        PERC[i] += rl == rr;
    }
}

int main(int argc, char *argv[])
{
    boundaries();
    double FIRE[N], PERC[N]; // average biggest cluster size
    int nsample = std::atoi(argv[1]);

    for (int i = 0; i < N; ++i)
    {
        FIRE[i] = 0;
        PERC[i] = 0;
    }

    for (int i = 0; i < nsample; i++)
    {
        permutation();
        percolate(FIRE, PERC);
    }

    for (int i = 0; i < N; ++i)
    {
        FIRE[i] /= nsample;
        PERC[i] /= nsample;
    }

    for (int i = 0; i < N; ++i)
        printf("%g %g\n", FIRE[i], PERC[i]);
    printf("\n");
    return EXIT_SUCCESS;
}
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <omp.h>
#include <climits>
#include "graph.h"

// n is the number of clusters
bool percolates(int L, graph_t *g, int c_size, int *c)
{
	const double XMIN = 0.0;
	const double XMAX = (double)(L - 1);

	// number of clusters
	double *xmin = (double *)malloc(sizeof(double) * c_size);
	double *xmax = (double *)malloc(sizeof(double) * c_size);
	double *ymin = (double *)malloc(sizeof(double) * c_size);
	double *ymax = (double *)malloc(sizeof(double) * c_size);

	for (int i = 0; i < c_size; ++i)
	{
		xmin[i] = ymin[i] = 1E99;
		xmax[i] = ymax[i] = -1E99;
	}

	bool perc = false;

	for (int i = 0; i < g->nnodes; ++i)
	{
		double x = g->nodes[i].x[0];
		double y = g->nodes[i].x[1];

		if (xmin[c[i]] > x)
			xmin[c[i]] = x;
		if (xmax[c[i]] < x)
			xmax[c[i]] = x;

		if (ymin[c[i]] > y)
			ymin[c[i]] = y;
		if (ymax[c[i]] < y)
			ymax[c[i]] = y;

		if ((xmin[c[i]] == XMIN && xmax[c[i]] == XMAX) ||
			(ymin[c[i]] == XMIN && ymax[c[i]] == XMAX))
		{
			perc = true;
			break;
		}
	}

	//dvec_write(c_size, xmin, "xmin.csv");
	//dvec_write(c_size, xmax, "xmax.csv");

	free(xmin);
	free(xmax);
	free(ymin);
	free(ymax);
	return perc;
}

double get_pc(const int L, const int seed, bool verbose)
{
	double tic, toc_percolates = 0, toc_clusters = 0, toc_grid = 0, toc_graph = 0;
	graph_t g;
	int *c = NULL, k = 0;
	double a = 0, b = 1, p, eps = 1.0 / (L * L);
	while ((b - a) > eps)
	{
		tic = omp_get_wtime();

		p = (a + b) / 2;

		if (verbose)
			printf("[%2d] p = %.7f ", ++k, p);

		// create graph
		graph_grid_2D(&g, L, p, seed);
		toc_graph += omp_get_wtime() - tic;

		if (verbose)
			printf(" toc %.3f secs\n", omp_get_wtime() - tic);

		tic = omp_get_wtime();
		c = (int *)malloc(sizeof(int) * g.nnodes);
		int n = graph_clusters(&g, c);
		toc_clusters += omp_get_wtime() - tic;

		// printf("nnodes = %d\n", g.nnodes);
		//graph_show(&g);
		//graph_write(&g, "perc");
		//ivec_write(g.nnodes, c, "perc_clusters.csv");

		tic = omp_get_wtime();
		if (percolates(L, &g, n, c))
			b = p;
		else
			a = p;
		free(c);
		graph_free(&g);
		toc_percolates += omp_get_wtime() - tic;
	}

	if (verbose)
	{
		printf("pc = %.7g +- %.1g\n", p, eps);
		printf("Timers (seconds)\n");
		printf("   graph      %g\n", toc_graph);
		printf("   clusters   %g\n", toc_clusters);
		printf("   percolates %g\n", toc_percolates);

		graph_grid_2D(&g, L, b, seed);
		c = (int *)malloc(sizeof(int) * g.nnodes);
		int n = graph_clusters(&g, c);
		graph_write(&g, "perc");
		ivec_write(g.nnodes, c, "perc_clusters.csv");
		free(c);
		graph_free(&g);
	}

	return p;
}

// the number of clusters Returns the biggest mass
int get_mass(int nnodes, int *c, int m_size, int *m)
{
	for (int i = 0; i < m_size; ++i)
		m[i] = 0;

	for (int i = 0; i < nnodes; ++i)
		m[c[i]]++;

	int max_m = 0;
	for (int i = 0; i < m_size; ++i)
		if (max_m < m[i])
			max_m = m[i];

	return max_m;
}

void mass_stats(int L, int nsample)
{
	int kmax = 2;
	double *p = (double *)malloc(sizeof(double) * kmax);
	int k = 0;
	for (double pk = 0.3; pk < 1;)
	{
		if (k >= kmax)
		{
			kmax *= 2;
			p = (double *)realloc(p, kmax * sizeof(double));
		}
		p[k++] = pk;
		if (pk < 0.5 || pk >= 0.6)
			pk += 0.01;
		else if (pk < 0.6)
			pk += 0.001;
	}
	int p_size = k;

	double *max_mass = (double *)malloc(sizeof(double) * p_size);

#pragma omp parallel for schedule(dynamic)
	for (int k = 0; k < p_size; ++k)
	{
		int *c = NULL;
		graph_t g;
		int *m = NULL, m_size; // mass of each cluster
		double tic = omp_get_wtime();
		double total_max_mass = 0;
		for (int seed = 0; seed < nsample; ++seed)
		{
			graph_grid_2D(&g, L, p[k], seed);
			c = (int *)malloc(sizeof(int) * g.nnodes);
			m_size = graph_clusters(&g, c);
			m = (int *)malloc(sizeof(int) * m_size);
			total_max_mass += get_mass(g.nnodes, c, m_size, m);
			free(m);
			free(c);
			graph_free(&g);
		}
		max_mass[k] = total_max_mass / (L * L * nsample);
		printf("mass_stats(%d, %d) p = %3.3f %3d of %d %5.4f secs tid %d\n",
			   L, nsample, p[k], k + 1, p_size, omp_get_wtime() - tic, omp_get_thread_num());
	}

	char fname[256];
	sprintf_s(fname, "perc_mass_L%d_N%d.csv", L, nsample);
	printf("Writing %s\n", fname);
	FILE *fid;
	fopen_s(&fid, fname, "w");
	fprintf(fid, "p,mass\n");
	for (int k = 0; k < p_size; ++k)
		fprintf(fid, "%g,%g\n", p[k], max_mass[k]);
	fclose(fid);
	free(max_mass);
}

int main(int argc, char *argv[])
{
	// calculates giant cluster
	// {
	// graph_t g;
	// int *c;
	// graph_grid_2D(&g, 100, 0.7, 0);
	// c = (int *)malloc(sizeof(int) * g.nnodes);
	// int c_size = graph_clusters(&g, c);
	// // graph_write(&g, "perc_70");
	// // ivec_write(g.nnodes, c, "perc_70_clusters.csv");
	// graph_free(&g);
	// free(c);
	// }

	// calculates critical p
	// {
	// 	const int seed = 0;
	// 	int L = 1000;
	// 	double a = 0.55;
	// 	double b = 0.65;
	// 	double p = get_pc(L, seed, a, b); // pc = 0.5927591 � 1e-06
	// }

	// calculates mass
	// {
	// 	int L = 4096;
	// 	int nsample = 100;
	// 	mass_stats(L, nsample);
	// }

	// write mass statistics (p vs M)
	// {
	// 	int L_size = 8;
	// 	int L[] = {32, 64, 128, 256, 512, 1024, 2048, 4096};
	// 	int nsample[] = {10000, 5000, 5000, 500, 500, 200, 200, 100};
	// 	for (int i = 0; i < L_size; i++)
	// 		mass_stats(L[i], nsample[i]);
	// }

	// write mass statics at pc (pc vs M)
	{
		const int L_size = 8;
		int L[] = {32, 64, 128, 256, 512, 1024, 2048, 4096};
		int nsample[] = {10000, 5000, 5000, 500, 500, 200, 200, 100};

		printf("Writing file perc_M.csv\n");

		FILE *fid = NULL;
		fopen_s(&fid, "perc_M.csv", "w");
		fprintf(fid, "L,M,P\n");
		for (int i = 0; i < L_size; i++)
		{
			double tic = omp_get_wtime();
			double total_m = 0;
			double total_p = 0;
#pragma omp parallel for reduction(+ \
								   : total_m, total_p)
			for (int seed = 0; seed < nsample[i]; ++seed)
			{
				double p = get_pc(L[i], seed, false);
				graph_t g;
				graph_grid_2D(&g, L[i], p, seed);
				int *c = (int *)malloc(sizeof(int) * g.nnodes);
				int m_size = graph_clusters(&g, c);
				int *m = (int *)malloc(sizeof(int) * m_size);
				total_m += (double)get_mass(g.nnodes, c, m_size, m);
				total_p += p;
				graph_free(&g);
				free(c);
				free(m);
			}
			double M = total_m / nsample[i];
			double P = total_p / nsample[i];
			fprintf(fid, "%d,%g,%g", L[i], M, P);
			printf("%6d %3.2e %.4f %.4f secs\n", L[i], M, P, omp_get_wtime() - tic);
		}
		fclose(fid);
	}

	return EXIT_SUCCESS;
}

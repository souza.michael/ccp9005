import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import networkx as nx
import sys

# prefix_perc = 'perc_%s0' % sys.argv[1]

def plot_mass():
    prefix = ('L32_N10000','L64_N5000','L128_N5000','L256_N500','L512_N500','L1024_N200','L2048_N200')
    m = {}
    p = 0.6
    for fname in prefix:    
       fname = '../cpp/perc_mass_%s.csv' % fname
       print('Reading %s' % fname)
       mass = pd.read_csv(fname)
       m[fname] = (mass.mass[mass.p==p].values)[0]
       label = fname.split('_')[-2].replace('L','L=')
       plt.plot(mass.p, mass.mass, label=label)
    plt.legend()
    plt.ylabel('P_L(L,p)')
    plt.xlabel('p')
    plt.show()
    L = []
    M = []
    for key in m.keys():
       L.append(int(((key.split('L'))[1]).split('_')[0]))
       M.append(m[key] * (L[-1]**2))
    print(L)
    print(M)
    plt.loglog(L,M,'-o')
    plt.xlabel('L')
    plt.ylabel('M(L,p)')
    plt.title('p = %f' % p)
    plt.show()
   
def plot_cluster(prefix):
    print('Reading nodes')
    nodes = pd.read_csv('../cpp/%s_nodes.csv' % prefix)
    print('Reading edges')
    edges = pd.read_csv('../cpp/%s_edges.csv' % prefix)
    print('Reading clusters')
    clusters = pd.read_csv('../cpp/%s_clusters.csv' % prefix)

    print('   nnodes ... %d' % len(nodes.i))
    print('   nedges ... %d' % (len(edges.i)/2))

    print('Getting largest cluster')
    c_size = np.max(clusters.i) + 1
    c_size = np.zeros(c_size, dtype=int)
    c = clusters.i
    for k in range(len(c)):
        c_size[c[k]] += 1

    lc_size = c_size[0]
    lc = 0
    for k in range(len(c_size)):
        if c_size[k] > lc_size:
            lc_size = c_size[k]
            lc = k
    print('   lc_size .. %d' % lc_size)
    print('   lc ....... %d' % lc)

    print('Plotting graph')
    x = nodes.x0
    y = nodes.x1
    plt.plot(x, y, 'm.', markersize=12)
    v = np.arange(0, len(nodes.i))
    v = v[clusters.i == lc]
    plt.plot(x[v], y[v], 'r.', markersize=12)
    plt.title(prefix)
    # plt.xlim(0, 9)
    # plt.ylim(0, 9)
    # plt.show()
    plt.savefig('fig_%s' % sys.argv[1])


# plot_cluster(perc_prefix)
plot_mass()

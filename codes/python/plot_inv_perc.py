import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab

fnodes = '../cpp/graph_nodes.csv'
fedges = '../cpp/graph_edges.csv'

# read nodes
p = np.genfromtxt('../cpp/vec_p.csv')
t = np.genfromtxt('../cpp/vec_t.csv')
data = pd.read_csv(fnodes)
nodes = {'i': [], 'x': [], 'y': [], 't': [], 'p': []}
for _, u in data.iterrows():
    nodes['i'].append(u.i)
    nodes['x'].append(u.x)
    nodes['y'].append(u.y)
    nodes['t'].append(t[u.i])
    nodes['p'].append(p[u.i])
nodes = pd.DataFrame.from_dict(nodes)

edges = pd.read_csv(fedges, index_col=None)
L = int(np.sqrt(len(nodes.i)))

P = np.zeros((L, L), dtype=float)
for _, u in nodes.iterrows():
    P[int(u.x), int(u.y)] = u.p
np.savetxt('tab_p.csv', P)

T = np.zeros((L, L), dtype=float)
for _, u in nodes.iterrows():
    T[int(u.x), int(u.y)] = u.t
np.savetxt('tab_t.csv', T)

cmap = matplotlib.cm.get_cmap('jet', 512)
fig, axs = plt.subplots(1, 2, figsize=(6, 6), constrained_layout=True)
psm = axs[0].pcolormesh(T, cmap=cmap, rasterized=True,
                        vmin=np.min(u.t), vmax=np.max(u.t))
fig.colorbar(psm, ax=axs[0])
psm = axs[1].pcolormesh(P, cmap=cmap, rasterized=True,
                        vmin=np.min(u.p), vmax=np.max(u.p))
fig.colorbar(psm, ax=axs[1])
plt.show()
